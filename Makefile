#/***************************************************************************
# IfsttarRouting
# 
# Get routing informations with various algorithms
#                             -------------------
#        begin                : 2012-04-12
#        copyright            : (C) 2012 by Oslandia
#        email                : hugo.mercier@oslandia.com
# ***************************************************************************/
# 
# CONFIGURATION

ifndef QGIS_VERSION
$(warning No QGIS_VERSION defined, default to QGIS 3, to override, run make with QGIS_VERSION=2 make)
QGIS_VERSION=3
endif

PLUGINNAME = tempus_qgis
VERSION=$(shell grep "version=" metadata.txt | cut -d'=' -f 2)

PY_FILES = altitude_profile.py \
	compat.py \
	compat3.py \
	config.py \
	consolelauncher.py \
	criterionchooser.py \
	routing_dialog.py \
	routing_dock.py \
	tempus_plugin.py \
	__init__.py \
	polygon_selection.py \
	result_selection.py \
	stepselector.py \
	wkb.py

EXTRAS = icon.png metadata.txt wps/wps_schemas pywps/wpstempus/wps_client.py pywps/wpstempus/history_file.py pywps/wpstempus/tempus_request.py \
	*.qml \
	tempus.qgs.tmpl \
	mouse_cross.png add.png remove.png turn_left.png turn_right.png roundabout.png

UI_FILES = ui_routing_dock.py ui_splash_screen.py ui_result_selection.py

RESOURCE_FILES = resources_rc.py

ifeq ($(QGIS_VERSION), 2)
QGIS_PLUGINS_DIR=$(HOME)/.qgis2/python/plugins
endif
ifeq ($(QGIS_VERSION), 3)
QGIS_PLUGINS_DIR=$(HOME)/.local/share/QGIS/QGIS3/profiles/default/python/plugins
endif
QGIS_PLUGIN_DIR=$(QGIS_PLUGINS_DIR)/$(PLUGINNAME)

default: compile

compile: $(UI_FILES) $(RESOURCE_FILES)

ifeq ($(QGIS_VERSION), 2)
%_rc.py : %.qrc
	pyrcc4 -o $*_rc.py  $<
else
%_rc.py : %.qrc
	pyrcc5 -o $*_rc.py  $<
endif

ifeq ($(QGIS_VERSION), 2)
ui_%.py : %.ui
	pyuic4 -o $@ $<
else
ui_%.py : %.ui
	pyuic5 --from-imports -o $@ $<
endif

%.qm : %.ts
	lrelease $<

# The deploy  target only works on unix like operating system where
# the Python plugin directory is located at:
# $HOME/.qgis2/python/plugins
deploy: clean derase compile
	mkdir -p $(QGIS_PLUGIN_DIR)
	cp -vf $(PY_FILES) $(UI_FILES) $(RESOURCE_FILES) $(QGIS_PLUGIN_DIR)
	cp -vfR $(EXTRAS) $(QGIS_PLUGIN_DIR)

# The dclean target removes compiled python files from plugin directory
# also delets any .svn entry
dclean:
	find $(QGIS_PLUGIN_DIR) -iname "*.pyc" -delete
	find $(QGIS_PLUGIN_DIR) -iname ".svn" -prune -exec rm -Rf {} \;

# The derase deletes deployed plugin
derase:
	rm -Rf $(QGIS_PLUGIN_DIR)

# The zip target deploys the plugin and creates a zip file with the deployed
# content. You can then upload the zip file on http://plugins.qgis.org
zip: derase deploy dclean 
	rm -f $(PLUGINNAME)-$(VERSION).zip
	cd $(QGIS_PLUGINS_DIR); zip -9r $(CURDIR)/$(PLUGINNAME)-$(VERSION).zip $(PLUGINNAME)

clean:
	rm $(UI_FILES) $(RESOURCE_FILES)

